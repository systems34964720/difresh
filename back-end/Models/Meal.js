const { Sequelize, DataTypes } = require('sequelize');
const sequelize = require('../db/dbConn.js');

const Meal = sequelize.define('Meal', {
  name: { type: DataTypes.STRING, allowNull: false },
  ingredients: { type: DataTypes.JSON, allowNull: false },
  instructions: { type: DataTypes.TEXT, allowNull: false },
  category: { type: DataTypes.STRING, allowNull: false },
}, {
  timestamps: true,
});

module.exports = Meal;