const { DataTypes } = require('sequelize');
const sequelize = require('../db/dbConn.js'); // Adjust the path if needed

const Recipe = sequelize.define(
  'Recipe',
  {
    name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    ingredients: {
      type: DataTypes.JSON, // Store ingredients as JSON
      allowNull: false,
    },
    instructions: {
      type: DataTypes.TEXT,
      allowNull: false,
    },
    category: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    cookingTime: {
      type: DataTypes.INTEGER, // Store cooking time in minutes
      allowNull: true,
      defaultValue: 0,
    },
    image: {
      type: DataTypes.STRING,
      allowNull: true,
    },
  },
  {
    timestamps: true, // Automatically adds createdAt and updatedAt columns
  }
);

module.exports = Recipe;


