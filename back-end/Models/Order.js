const { DataTypes } = require('sequelize');
const sequelize = require('./config/database');
const Recipe = require('./models/Recipe');

// Define Order model
const Order = sequelize.define('Order', {
  userId: { type: DataTypes.INTEGER, allowNull: false }, // Assuming users are identified by an ID
  status: { type: DataTypes.STRING, defaultValue: 'Pending' }, // e.g., Pending, Completed
});

// Define many-to-many relationship between Orders and Recipes
const OrderRecipe = sequelize.define('OrderRecipe', {});

Order.belongsToMany(Recipe, { through: OrderRecipe });
Recipe.belongsToMany(Order, { through: OrderRecipe });

module.exports = { Order, OrderRecipe };

