const asyncHandler = require('express-async-handler');
const Order = require('../models/Order');
const User = require('../models/User');
const Meal = require('../models/Meal');

// @desc    Get all orders for logged in user
// @route   GET /api/orders
// @access  Private
const getOrders = asyncHandler(async (req, res) => {
  const orders = await Order.findAll({ where: { userId: req.user.id }, include: [Meal] });
  res.json(orders);
});

// @desc    Create a new order
// @route   POST /api/orders
// @access  Private
const createOrder = asyncHandler(async (req, res) => {
  const { mealId, quantity, deliveryDate } = req.body;

  const meal = await Meal.findByPk(mealId);
  if (!meal) {
    res.status(404);
    throw new Error('Meal not found');
  }

  const order = await Order.create({
    userId: req.user.id,
    mealId,
    quantity,
    deliveryDate
  });

  res.status(201).json(order);
});

// @desc    Get a single order by ID
// @route   GET /api/orders/:id
// @access  Private
const getOrder = asyncHandler(async (req, res) => {
  const order = await Order.findByPk(req.params.id, { include: [Meal] });
  if (order.userId !== req.user.id) {
    res.status(401);
    throw new Error('Not authorized to view this order');
  }
  res.json(order);
});

module.exports = { getOrders, createOrder, getOrder };
