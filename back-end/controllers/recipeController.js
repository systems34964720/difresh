const asyncHandler = require('express-async-handler');
const { Op } = require('sequelize'); // Import Sequelize operators for advanced queries
const Recipe = require('../models/Recipe'); // Import the Recipe model
const DB = require('../db/dbConn');

// @desc    Get all recipes or filter by dietary option and cooking time
// @route   GET /api/recipes
// @access  Private
const getRecipes = asyncHandler(async (req, res) => {
  const { dietary, time } = req.query; // Extract filters from query parameters

  // Build the query conditions
  const conditions = {};

  // Add dietary filter
  if (dietary) {
    conditions.category = dietary;
  }

  // Add cooking time filter
  if (time) {
    if (time === '<30') {
      conditions.cookingTime = { [Op.lt]: 30 }; // Cooking time less than 30 mins
    } else if (time === '30-50') {
      conditions.cookingTime = { [Op.between]: [30, 50] }; // Cooking time between 30 and 50 mins
    } else if (time === '>50') {
      conditions.cookingTime = { [Op.gt]: 50 }; // Cooking time greater than 50 mins
    }
  }

  // Fetch recipes based on conditions
  const recipes = await Recipe.findAll({
    where: conditions, // Use the constructed conditions object
  });

  res.json(recipes);
});

// @desc    Create a new recipe
// @route   POST /api/recipes
// @access  Private
const createRecipe = asyncHandler(async (req, res) => {
  const { name, ingredients, instructions, category, cookingTime } = req.body;

  // Create a new recipe in the database
  const recipe = await Recipe.create({
    name,
    ingredients: JSON.stringify(ingredients), // Store ingredients as a JSON string if it's an array
    instructions,
    category,
    cookingTime, // Add cookingTime to the recipe
  });

  res.status(201).json(recipe);
});

// @desc    Get a single recipe by ID
// @route   GET /api/recipes/:id
// @access  Private
const getRecipe = asyncHandler(async (req, res) => {
  const recipe = await Recipe.findByPk(req.params.id);

  if (!recipe) {
    res.status(404);
    throw new Error('Recipe not found');
  }

  res.json(recipe);
});

// @desc    Update a recipe
// @route   PUT /api/recipes/:id
// @access  Private
const updateRecipe = asyncHandler(async (req, res) => {
  const { name, ingredients, instructions, category, cookingTime } = req.body;

  const recipe = await Recipe.findByPk(req.params.id);

  if (!recipe) {
    res.status(404);
    throw new Error('Recipe not found');
  }

  // Update the recipe fields
  recipe.name = name || recipe.name;
  recipe.ingredients = ingredients ? JSON.stringify(ingredients) : recipe.ingredients;
  recipe.instructions = instructions || recipe.instructions;
  recipe.category = category || recipe.category;
  recipe.cookingTime = cookingTime || recipe.cookingTime;

  const updatedRecipe = await recipe.save();
  res.json(updatedRecipe);
});

// @desc    Delete a recipe
// @route   DELETE /api/recipes/:id
// @access  Private
const deleteRecipe = asyncHandler(async (req, res) => {
  const recipe = await Recipe.findByPk(req.params.id);

  if (!recipe) {
    res.status(404);
    throw new Error('Recipe not found');
  }

  await recipe.destroy();
  res.json({ message: 'Recipe removed' });
});

module.exports = { getRecipes, createRecipe, getRecipe, updateRecipe, deleteRecipe };
const axios = require('axios');
const Recipe = require('./models/Recipe'); // Your Sequelize model
const sequelize = require('./config/database');

const fetchAndStoreRecipes = async () => {
  
  try {
    // Connect to your database
    await sequelize.authenticate();
    console.log('Database connected!');

    // Sync database to ensure the table exists
    await sequelize.sync({ alter: true }); // Use `force: true` only if you want to reset the table

    // Example API URL (Replace with your actual API URL)
    const API_URL = 'https://api.edamam.com/api/recipes/v2?type=public&app_id=21e0b7ea&app_key=238825a094b69f9f29ed807f2b738105&diet=balanced'; 

    // Fetch recipes from the API
    const response = await axios.get(API_URL);

    if (response.data && Array.isArray(response.data.recipes)) {
      const recipes = response.data.recipes;

      // Iterate over recipes and store each in the database
      for (const recipe of recipes) {
        await DB.saveRecipe({
          name: recipe.label || 'Untitled', // Ensure fallback for missing fields
          ingredients: recipe.ingredients || [],
          instructions: recipe.instructions || 'Instructions not available.',
          category: recipe.category || 'Unknown',
          cookingTime: recipe.totalTime || 0, // Default to 0 if cooking time is not provided
        });
      }

      console.log('Recipes successfully stored in the database.');
    } else {
      console.error('Unexpected API response format:', response.data);
    }
  } catch (error) {
    console.error('Error fetching or storing recipes:', error.message);
  } finally {
    // Close the database connection
    await sequelize.close();
    console.log('Database connection closed.');
  }
};

// Call the function
fetchAndStoreRecipes();