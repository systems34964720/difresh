const Meal = require('../models/Meal');

const getMeals = async (req, res) => {
  // Fetching meals logic
};

const createMeal = async (req, res) => {
  // Creating a meal logic
};

module.exports = { getMeals, createMeal };
