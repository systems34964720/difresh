const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const User = require('../models/User');

const register = async (req, res) => {
  // Registration logic
};

const login = async (req, res) => {
  // Login logic
};

module.exports = { register, login };
