const express = require('express');
const { getMeals, createMeal } = require('../controllers/mealController');
const { protect } = require('../middleware/authMiddleware');
const router = express.Router();

router.get('/', protect, getMeals);
router.post('/', protect, createMeal);

module.exports = router;
