const asyncHandler = require('express-async-handler');
const { Order, OrderRecipe, Recipe } = require('../models'); // Import models

// @desc    Get all orders for a user
// @route   GET /api/orders
// @access  Private
const getOrders = asyncHandler(async (req, res) => {
  const { userId } = req.user; // Assuming user info is stored in `req.user`
  const orders = await Order.findAll({
    where: { userId },
    include: Recipe,
  });

  res.json(orders);
});

// @desc    Add a recipe to an order
// @route   POST /api/orders/:orderId/recipes
// @access  Private
const addRecipeToOrder = asyncHandler(async (req, res) => {
  const { orderId } = req.params;
  const { recipeId } = req.body;

  const order = await Order.findByPk(orderId);
  const recipe = await Recipe.findByPk(recipeId);

  if (!order || !recipe) {
    res.status(404);
    throw new Error('Order or Recipe not found');
  }

  await order.addRecipe(recipe);
  res.json({ message: `Recipe ${recipe.name} added to order ${orderId}` });
});

// @desc    Create a new order
// @route   POST /api/orders
// @access  Private
const createOrder = asyncHandler(async (req, res) => {
  const { userId } = req.user; // Assuming user info is stored in `req.user`

  const order = await Order.create({ userId });
  res.status(201).json(order);
});

module.exports = { getOrders, addRecipeToOrder, createOrder };

