const mysql = require('mysql2');

const conn = mysql.createConnection({
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASS,
    database: process.env.DB_DATABASE,
});

conn.connect((err) => {
    if (err) {
        console.log("ERROR: " + err.message);
        return;
    }
    console.log('Connection established');
});

let dataPool = {};

// Authenticate user by email and password
dataPool.AuthUser = (email, password) => {
    return new Promise((resolve, reject) => {
        conn.query(
            'SELECT * FROM User WHERE email = ? AND password = ?',
            [email, password],
            (err, res) => {
                if (err) return reject(err);
                return resolve(res);
            }
        );
    });
};

// Add a new user
dataPool.AddUser = (name, surname, password, email) => {
    return new Promise((resolve, reject) => {
        conn.query(
            'INSERT INTO User (name, surname, password, email) VALUES (?, ?, ?, ?)',
            [name, surname, password, email],
            (err, res) => {
                if (err) return reject(err);
                return resolve(res);
            }
        );
    });
};

// Save a new recipe
dataPool.saveRecipe = (recipe) => {
    return new Promise((resolve, reject) => {
        conn.query(
            'INSERT INTO RECIPES (title, calories, image, cookingTime, mealType) VALUES (?, ?, ?, ?, ?)',
            [recipe.title, recipe.calories, recipe.image, recipe.totalTime, recipe.mealType],
            (err, res) => {
                if (err) return reject(err);
                resolve(res);
            }
        );
    });
};

// Save a new ingredient (use auto-incremented ID for the ingredient)
dataPool.saveIngredient = (ingredient) => {
    return new Promise((resolve, reject) => {
        conn.query(
            'INSERT INTO INGREDIENTS (name, quantity, measure) VALUES (?, ?, ?)',
            [ingredient.name, ingredient.quantity || null, ingredient.measure || null],
            (err, res) => {
                if (err) return reject(err);
                resolve(res);
            }
        );
    });
};

dataPool.AddUser=(name,surname,password,email)=>{
  return new Promise ((resolve, reject)=>{
    conn.query(`INSERT INTO User (name,surname,password,email) VALUES (?,?,?,?)`, [name,surname,password,email], (err,res)=>{
      if(err){return reject(err)}
      return resolve(res)
    })
  })
}


module.exports = dataPool;
