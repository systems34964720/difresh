import React, { useState, useEffect } from 'react';
import { getRecipes } from '../services/recipeService';
import Cookies from 'universal-cookie';
import HomeView from '../CustomComponents/HomeView';
import ProfileView from '../CustomComponents/ProfileView';

const cookies = new Cookies();

const RecipesPage = () => {
  const [recipes, setRecipes] = useState([]);
  const [filteredRecipes, setFilteredRecipes] = useState([]); // State for filtered recipes
  const [state, setState] = useState(0);
  const [selectedRecipe, setSelectedRecipe] = useState(null); // State to track the selected recipe
  const [timeFilters, setTimeFilters] = useState([]); // State for cooking time filters

  useEffect(() => {
    const fetchRecipes = async () => {
      try {
        const data = await getRecipes();
        setRecipes(data);
        setFilteredRecipes(data); // Initially, show all recipes
        setState(1); // Example state update, change as needed
      } catch (error) {
        console.error('Error fetching recipes:', error);
      }
    };

    fetchRecipes();
  }, []);

  const handleTimeFilterChange = (e) => {
    const value = e.target.value;
    setTimeFilters((prevFilters) =>
      e.target.checked ? [...prevFilters, value] : prevFilters.filter((f) => f !== value)
    );
  };

  useEffect(() => {
    const filterRecipes = () => {
      if (timeFilters.length === 0) {
        setFilteredRecipes(recipes); // No filters, show all recipes
      } else {
        const filtered = recipes.filter((recipe) => {
          const totalTime = recipe.recipe.totalTime || 0; // Default to 0 if totalTime is not available
          if (timeFilters.includes('<30') && totalTime < 30) return true;
          if (timeFilters.includes('30-50') && totalTime >= 30 && totalTime <= 50) return true;
          if (timeFilters.includes('>50') && totalTime > 50) return true;
          return false;
        });
        setFilteredRecipes(filtered);
      }
    };

    filterRecipes();
  }, [timeFilters, recipes]);

  const handleShowRecipe = (recipe) => {
    setSelectedRecipe(recipe === selectedRecipe ? null : recipe); // Toggle recipe details
  };

  return (
    <div>
      {/* Conditional rendering */}
      {state === 1 ? <HomeView /> : <ProfileView />}

      <h1>Recipes</h1>

      {/* Filter Section */}
      <div>
        <h3>Filter by Cooking Time:</h3>
        <label>
          <input
            type="checkbox"
            value="<30"
            onChange={handleTimeFilterChange}
          />
          Less than 30 mins
        </label>
        <br />
        <label>
          <input
            type="checkbox"
            value="30-50"
            onChange={handleTimeFilterChange}
          />
          30-50 mins
        </label>
        <br />
        <label>
          <input
            type="checkbox"
            value=">50"
            onChange={handleTimeFilterChange}
          />
          More than 50 mins
        </label>
      </div>

      <ul>
        {filteredRecipes.map((recipe, index) => (
          <li key={index}> {/* Replace with a unique ID if available */}
            <p><strong>{recipe.recipe.label}</strong></p>
            <p>
              <img
                src={recipe.recipe.image}
                alt={recipe.recipe.label}
                width="100"
                height="120"
              />
            </p>
            <button className="button" onClick={() => handleShowRecipe(recipe)}>
              {selectedRecipe === recipe ? "Hide Recipe" : "Show Recipe"}
            </button>

            {selectedRecipe === recipe && (
              <div>
                <h3>Ingredients:</h3>
                <ul>
                  {recipe.recipe.ingredientLines.map((ingredient, i) => (
                    <li key={i}>{ingredient}</li>
                  ))}
                </ul>
                <h3>Instructions:</h3>
                <p>{recipe.instructions || "Instructions not available."}</p>
                <p><strong>Time:</strong> {recipe.recipe.totalTime || 'N/A'} minutes</p>
                <p><strong>Dish type:</strong> {recipe.recipe.dishType || 'Unknown'}</p>
              </div>
            )}
          </li>
        ))}
      </ul>
    </div>
  );
};

export default RecipesPage;

