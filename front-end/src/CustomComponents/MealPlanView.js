import React, { useState, useEffect } from 'react';
import axios from 'axios';

const MealPlanView = () => {
  const [recipes, setRecipes] = useState([]); // Recipes from Edamam API
  const [mealPlan, setMealPlan] = useState('');
  const [planPrice, setPlanPrice] = useState(0);
  const [selectedRecipes, setSelectedRecipes] = useState([]);
  const [loading, setLoading] = useState(false);

  const EDAMAM_API_URL = 'https://api.edamam.com/api/recipes/v2';
  const APP_ID = '21e0b7ea';
  const APP_KEY = '238825a094b69f9f29ed807f2b738105';

  // Fetch recipes from Edamam API
  const fetchRecipes = async () => {
    setLoading(true);
    try {
      const response = await axios.get(EDAMAM_API_URL, {
        params: {
          type: 'public',
          app_id: APP_ID,
          app_key: APP_KEY,
          diet: 'balanced',
        },
      });
      const recipeData = response.data.hits.map((hit) => ({
        id: hit.recipe.uri, // Unique ID from URI
        name: hit.recipe.label,
        ingredients: hit.recipe.ingredientLines,
        instructions: hit.recipe.url, // Use recipe URL for instructions
        image: hit.recipe.image,
      }));
      setRecipes(recipeData);
    } catch (error) {
      console.error('Error fetching recipes:', error);
    }
    setLoading(false);
  };

  // Fetch recipes on component load
  useEffect(() => {
    fetchRecipes();
  }, []);

  // Handle meal plan selection
  const handleMealPlanChange = (e) => {
    const selectedPlan = e.target.value;

    switch (selectedPlan) {
      case '3':
        setPlanPrice(20);
        break;
      case '4':
        setPlanPrice(25);
        break;
      case '5':
        setPlanPrice(30);
        break;
      default:
        setPlanPrice(0);
    }

    setMealPlan(selectedPlan);
    setSelectedRecipes([]); // Reset selected recipes when a new plan is chosen
  };

  // Handle recipe selection
  const handleRecipeSelection = (recipeId) => {
    if (selectedRecipes.includes(recipeId)) {
      // Remove the recipe if it's already selected
      setSelectedRecipes(selectedRecipes.filter((id) => id !== recipeId));
    } else {
      // Add the recipe if it's not selected
      if (selectedRecipes.length < parseInt(mealPlan)) {
        setSelectedRecipes([...selectedRecipes, recipeId]);
      } else {
        alert(`You can only select ${mealPlan} recipes for this plan.`);
      }
    }
  };

  // Submit the selected meal plan (optional backend integration)
  const handleMealPlanSubmit = async () => {
    if (selectedRecipes.length !== parseInt(mealPlan)) {
      alert(`Please select exactly ${mealPlan} recipes.`);
      return;
    }

    try {
      // Replace with your backend API endpoint to save the meal plan
      const response = await axios.post('/api/meal-plans', {
        mealPlan,
        price: planPrice,
        recipes: selectedRecipes,
      });
      alert('Meal plan saved successfully!');
    } catch (error) {
      console.error('Error saving meal plan:', error);
      alert('Failed to save meal plan.');
    }
  };

  return (
    <div>
      <h1>Meal Plan Options</h1>
      <div>
        <label htmlFor="mealPlan">Select a Meal Plan:</label>
        <select id="mealPlan" value={mealPlan} onChange={handleMealPlanChange}>
          <option value="">--Select Plan--</option>
          <option value="3">3 Meals - €20</option>
          <option value="4">4 Meals - €25</option>
          <option value="5">5 Meals - €30</option>
        </select>
      </div>

      {mealPlan && (
        <div>
          <h2>Selected Plan</h2>
          <p>
            {mealPlan} meals per week for €{planPrice}
          </p>

          <h3>Select Your Recipes</h3>
          {loading ? (
            <p>Loading recipes...</p>
          ) : (
            <ul>
              {recipes.map((recipe) => (
                <li key={recipe.id}>
                  <input
                    type="checkbox"
                    id={`recipe-${recipe.id}`}
                    value={recipe.id}
                    checked={selectedRecipes.includes(recipe.id)}
                    onChange={() => handleRecipeSelection(recipe.id)}
                  />
                  <label htmlFor={`recipe-${recipe.id}`}>
                    <strong>{recipe.name}</strong>
                    <p>Ingredients: {recipe.ingredients.join(', ')}</p>
                    <p>
                      Instructions:{' '}
                      <a href={recipe.instructions} target="_blank" rel="noopener noreferrer">
                        View Instructions
                      </a>
                    </p>
                    <img src={recipe.image} alt={recipe.name} style={{ width: '100px' }} />
                  </label>
                </li>
              ))}
            </ul>
          )}
          <p>
            {selectedRecipes.length}/{mealPlan} recipes selected.
          </p>
          <button onClick={handleMealPlanSubmit}>Save Meal Plan</button>
        </div>
      )}
    </div>
  );
};

export default MealPlanView;


