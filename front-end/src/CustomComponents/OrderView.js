import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { getOrders, createOrder } from '../services/orderService';

// Orders Page Component
const OrdersPage = () => {
  const [orders, setOrders] = useState([]);
  const [mealId, setMealId] = useState('');
  const [quantity, setQuantity] = useState(1);
  const [deliveryDate, setDeliveryDate] = useState('');

  useEffect(() => {
    const fetchOrders = async () => {
      try {
        const data = await getOrders();
        setOrders(data);
      } catch (error) {
        console.error('Error fetching orders:', error);
      }
    };
    fetchOrders();
  }, []);

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      await createOrder({ mealId, quantity, deliveryDate });
      setMealId('');
      setQuantity(1);
      setDeliveryDate('');
      // Refresh orders
      const data = await getOrders();
      setOrders(data);
    } catch (error) {
      console.error('Error creating order:', error);
    }
  };

  return (
    <div>
      <h1>Orders</h1>
      <form onSubmit={handleSubmit}>
        <div>
          <label>Meal ID</label>
          <input
            type="text"
            value={mealId}
            onChange={(e) => setMealId(e.target.value)}
          />
        </div>
        <div>
          <label>Quantity</label>
          <input
            type="number"
            value={quantity}
            onChange={(e) => setQuantity(e.target.value)}
          />
        </div>
        <div>
          <label>Delivery Date</label>
          <input
            type="date"
            value={deliveryDate}
            onChange={(e) => setDeliveryDate(e.target.value)}
          />
        </div>
        <button type="submit">Create Order</button>
      </form>
      <ul>
        {orders.map((order) => (
          <li key={order.id}>
            <p>Meal ID: {order.mealId}</p>
            <p>Quantity: {order.quantity}</p>
            <p>Delivery Date: {order.deliveryDate}</p>
          </li>
        ))}
      </ul>
    </div>
  );
};

// Recipe Order Component
const RecipeOrder = () => {
  const [recipes, setRecipes] = useState([]);
  const [orderId, setOrderId] = useState(null);

  // Fetch recipes from the API
  const fetchRecipes = async () => {
    try {
      const response = await axios.get('/api/recipes'); // Replace with your actual endpoint
      setRecipes(response.data);
    } catch (error) {
      console.error('Error fetching recipes:', error.message);
    }
  };

  // Create a new order
  const createNewOrder = async () => {
    try {
      const response = await axios.post('/api/orders');
      setOrderId(response.data.id);
      alert('New order created!');
    } catch (error) {
      console.error('Error creating order:', error.message);
    }
  };

  // Add a recipe to the order
  const addRecipeToOrder = async (recipeId) => {
    if (!orderId) {
      alert('No active order found. Please create an order first.');
      return;
    }

    try {
      await axios.post(`/api/orders/${orderId}/recipes`, { recipeId });
      alert('Recipe added to order!');
    } catch (error) {
      console.error('Error adding recipe to order:', error.message);
    }
  };

  // Fetch recipes when the component is mounted
  useEffect(() => {
    fetchRecipes();
  }, []);

  return (
    <div>
      <h1>Recipe Selection</h1>
      <button onClick={createNewOrder}>Create New Order</button>
      <div id="recipes-container">
        {recipes.map((recipe) => (
          <div key={recipe.id} className="recipe">
            <p>{recipe.name}</p>
            <button onClick={() => addRecipeToOrder(recipe.id)}>Add to Order</button>
          </div>
        ))}
      </div>
    </div>
  );
};

// Combined OrderView Component
const OrderView = () => {
  return (
    <div>
      <OrdersPage />
      <RecipeOrder />
    </div>
  );
};

export default OrderView;
