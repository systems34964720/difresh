import { Component } from "react";
import {SIGNUP, LOGIN, HOME, LOGOUT, MEALPLAN, RECIPES, SHOPPINGCART, PROFILE, ORDER, RECIPEPAGE } from "./Utils/Constants"
import HomeView from "./CustomComponents/HomeView";
//import AboutView from "./CustomComponents/AboutView";
//import NoviceView from "./CustomComponents/NoviceView";
//import AddNovicaView from "./CustomComponents/AddNovicaView";
import MealPlanView from "./CustomComponents/MealPlanView";
import OrderView from "./CustomComponents/OrderView";
import ProfileView from "./CustomComponents/ProfileView";
import SignupView from "./CustomComponents/SignupView";
import LoginView from "./CustomComponents/LoginView";
import RecipesView from "./CustomComponents/RecipesView";
import ShoppingCartView from "./CustomComponents/ShoppingCartView";
import RecipesPage from './pages/recipePage';
//import SingleNovicaView from "./CustomComponents/SingleNovicaView";
//import FilesUploadComponent from "./CustomComponents/FilesUpload";
import axios from "axios";
import { API_URL } from "./Utils/Configuration";
import Cookies from 'universal-cookie';
const cookies = new Cookies();

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      CurrentPage: HOME,
      Novica: 1,
      status: {
        success: null,
        msg: ""
      },
      user: null
    };
  }

  QGetView(state) {
    const page = state.CurrentPage;
    switch (page) {
      //  case ADDNEW:
      //    return <AddNovicaView />;
       case SIGNUP:
         return <SignupView />;
      case LOGIN:
        return <LoginView QUserFromChild={this.QSetLoggedIn} />;
        case ORDER:
        return <OrderView QUserFromChild={this.QSetLoggedIn} />;
        case RECIPES:
        return <RecipesView QUserFromChild={this.QSetLoggedIn} />;
        case SHOPPINGCART:
        return <ShoppingCartView QUserFromChild={this.QSetLoggedIn} />;
        case PROFILE:
        return <ProfileView QUserFromChild={this.QSetLoggedIn} />;
        case RECIPEPAGE:
          return <RecipesPage QUserFromChild={this.QSetLoggedIn} />;
      //case LOGOUT:
        //return <HomeView />;
      // case UPLOAD:
      //   return <FilesUploadComponent />;
        case MEALPLAN:
        return <MealPlanView />;
      //case NOVICA:
        //return <SingleNovicaView data={state.Novica} QIDFromChild={this.QSetView} />;
      default:
        return <HomeView />;
    }
  };

  QSetView = (obj) => {
    this.setState(this.state.status = { success: null, msg: "" })

    console.log("QSetView");
    this.setState({
      CurrentPage: obj.page,
      Novica: obj.id || 0
    });
  };

  render() {
    return (
      <div id="APP" className="container">
        <div id="menu" className="row">
          <nav className="navbar navbar-expand-lg navbar-dark bg-primary">
            <div className="container-fluid">
              <a
                //onClick={() => this.QSetView({ page: "home" })}
                onClick={this.QSetView.bind(this, { page: "home" })}
                className="navbar-brand"
                href="#"
              >
                Home
              </a>
              <button
                className="navbar-toggler"
                type="button"
                data-bs-toggle="collapse"
                data-bs-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent"
                aria-expanded="false"
                aria-label="Toggle navigation"
              >
                <span className="navbar-toggler-icon"></span>
              </button>

              <div
                className="collapse navbar-collapse"
                id="navbarSupportedContent"
              >
                <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                  {/* <li className="nav-item">
                    <a
                      // onClick={() => this.QSetView({ page: ABOUT })}
                      onClick={this.QSetView.bind(this, { page: ABOUT })}
                      className="nav-link "
                      href="#"
                    >
                      About
                    </a>
                  </li>\ */}

<li className="nav-item">
                    <a
                      // onClick={() => this.QSetView({ page: ABOUT })}
                      onClick={this.QSetView.bind(this, { page: PROFILE })}
                      className="nav-link "
                      href="#"
                    >
                      Profile
                    </a>
                  </li>

                  

                  <li className="nav-item">
                    <a
                      // onClick={() => this.QSetView({ page: ABOUT })}
                      onClick={this.QSetView.bind(this, { page: MEALPLAN })}
                      className="nav-link "
                      href="#"
                    >
                      MealPlan
                    </a>
                  </li>

                  <li className="nav-item">
                    <a
                      // onClick={() => this.QSetView({ page: ABOUT })}
                      onClick={this.QSetView.bind(this, { page: RECIPEPAGE })}
                      className="nav-link "
                      href="#"
                    >
                      recipePage
                    </a>
                  </li>

                  <li className="nav-item">
                    <a
                      // onClick={() => this.QSetView({ page: ABOUT })}
                      onClick={this.QSetView.bind(this, { page: ORDER })}
                      className="nav-link "
                      href="#"
                    >
                      Order
                    </a>
                  </li>

                  <li className="nav-item">
                    <a
                      // onClick={() => this.QSetView({ page: ABOUT })}
                      onClick={this.QSetView.bind(this, { page: SHOPPINGCART })}
                      className="nav-link "
                      href="#"
                    >
                      ShoppingCart
                    </a>
                  </li>

                  <li className="nav-item">
                    <a
                      // onClick={() => this.QSetView({ page: ABOUT })}
                      onClick={this.QSetView.bind(this, { page: RECIPES })}
                      className="nav-link "
                      href="#"
                    >
                      Recipes
                    </a>
                  </li>

                  {/* <li className="nav-item">
                    <a
                      // onClick={() => this.QSetView({ page: NOVICE })}
                      onClick={this.QSetView.bind(this, { page: NOVICE })}
                      className="nav-link "
                      href="#"
                    >
                      News
                    </a>
                  </li> */}

                  {/* <li className="nav-item">
                    <a
                      //onClick={() => this.QSetView({ page: ADDNEW })}
                      onClick={this.QSetView.bind(this, { page: ADDNEW })}
                      className="nav-link"
                      href="#"
                    >
                      Add news
                    </a>
                  </li> */}


                  {/* <li className="nav-item">
                    <a
                      //onClick={() => this.QSetView({ page: ADDNEW })}
                      onClick={this.QSetView.bind(this, { page: UPLOAD })}
                      className="nav-link"
                      href="#"
                    >
                      Upload
                    </a>
                  </li> */}

                  <li className="nav-item">
                    <a
                      //onClick={() => this.QSetView({ page: SIGNUP })}
                      onClick={this.QSetView.bind(this, { page: SIGNUP })}
                      className="nav-link "
                      href="#"
                    >
                      Sign up
                    </a>
                  </li>

                 <li className="nav-item" ><a onClick={this.QSetView.bind(this, { page: LOGIN })}
                      className="nav-link " href="#"> Login </a>
                 </li>
                
                </ul>
              </div>
            </div>
          </nav>
        </div>

        <div id="viewer" className="row container">
          {this.QGetView(this.state)}
        </div>
      </div>
    );
  }
}

export default App;
