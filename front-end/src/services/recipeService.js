import axios from 'axios';

const API_URL = 'https://api.edamam.com/api/recipes/v2?type=public&app_id=21e0b7ea&app_key=238825a094b69f9f29ed807f2b738105&diet=balanced';

export const getRecipes = async () => {
  const response = await axios.get(API_URL);
  return response.data.hits;
};

export const createRecipe = async (recipeData) => {
  const response = await axios.post(API_URL, recipeData, {
    headers: { Authorization: `Bearer ${JSON.parse(localStorage.getItem('Email')).token}` }
  });
  return response.data;
};

export const getRecipe = async (id) => {
  const response = await axios.get(`${API_URL}/${id}`, {
    headers: { Authorization: `Bearer ${JSON.parse(localStorage.getItem('Email')).token}` }
  });
  return response.data;
};

export const updateRecipe = async (id, recipeData) => {
  const response = await axios.put(`${API_URL}/${id}`, recipeData, {
    headers: { Authorization: `Bearer ${JSON.parse(localStorage.getItem('Email')).token}` }
  });
  return response.data;
};

export const deleteRecipe = async (id) => {
  const response = await axios.delete(`${API_URL}/${id}`, {
    headers: { Authorization: `Bearer ${JSON.parse(localStorage.getItem('Email')).token}` }
  });
  return response.data;
};
