import axios from 'axios';

const API_URL = 'http://88.200.63.148:8089';

export const getMeals = async () => {
  const response = await axios.get(API_URL, {
    headers: { Authorization: `Bearer ${JSON.parse(localStorage.getItem('user')).token}` }
  });
  return response.data;
};

export const createMeal = async (mealData) => {
  const response = await axios.post(API_URL, mealData, {
    headers: { Authorization: `Bearer ${JSON.parse(localStorage.getItem('user')).token}` }
  });
  return response.data;
};
