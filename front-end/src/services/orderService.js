import axios from 'axios';

// Function to fetch all orders
export const getOrders = async () => {
  try {
    const response = await axios.get('/api/orders');
    return response.data;
  } catch (error) {
    console.error('Error fetching orders:', error.message);
    throw error;
  }
};

// Function to create a new order
export const createOrder = async (orderData) => {
  try {
    const response = await axios.post('/api/orders', orderData);
    return response.data;
  } catch (error) {
    console.error('Error creating order:', error.message);
    throw error;
  }
};
